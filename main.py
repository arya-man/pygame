import math
from config import *
        
    
tup_255=(255,255,255)
tup_200=(200,250)

def game_end_text():
    if sp1 > sp2:
        winner_text1 = winner_font.render("Player 1 wins", True, tup_255 )
        screen.blit(winner_text1, tup_200)
        pygame.display.update()
        pygame.time.wait(600)
    elif sp2 > sp1:
        winner_text2 = winner_font.render("Player 2 wins", True, tup_255)
        screen.blit(winner_text2, tup_200)
        pygame.display.update()
        pygame.time.wait(600)
    else:
        winner_text3 = winner_font.render("TIE", True, tup_255)
        screen.blit(winner_text3, tup_200)
        pygame.display.update()
        pygame.time.wait(600)




def show_scorep1(x, y):
    tup_xy=(x, y)
    global sp1
    if count == 0:
        scorep1 = font.render("Score : " + str((score_valuep1 - t1p1)), True, tup_255)
        assert isinstance(screen, object)
        screen.blit(scorep1, tup_xy)
    else:
        scorep1 = font.render("Score : " + str((r21 + prevsp1 - t2p1 - t1p1)), True, tup_255)
        assert isinstance(screen, object)
        screen.blit(scorep1, tup_xy)
    sp1 = r21 + prevsp1 - t2p1


def show_scorep2(x, y):
    tup_xy=(x, y)
    global sp2
    if count == 1:
        scorep2 = font.render("Score : " + str((score_valuep2 - t1p2)), True, tup_255)
        assert isinstance(screen, object)
        screen.blit(scorep2, tup_xy)
    else:
        scorep2 = font.render("Score : " + str((r22 + prevsp2 - t2p2 - t1p2)), True, tup_255)
        assert isinstance(screen, object)
        screen.blit(scorep2, tup_xy)
    sp2 = r22 + prevsp2 - t2p2

sp_two=0
strip_color=1600

def isCollision(enemyX, enemyY, playerX, playerY):
    check_var=0
    distance = math.sqrt((math.pow(enemyY - playerY, 2) + math.pow(enemyX - playerX, 2)))

    if distance >=64:
        return False

    else:
        return True
    


def enemy(x, y, i):
    tup_xy=(x, y)
    screen.blit(enemyImg[i], tup_xy)


def enemy2(x, y, i):
    tup_xy=(x, y)
    screen.blit(enemyImg[i], tup_xy)


def player(x, y):
    tup_xy=(x, y)
    screen.blit(playerImg, tup_xy)

running = True


score1=[15,30,45,60,75,5,20,35,50,65,80]
score2=[75,60,45,30,15,80,65,50,35,20,5]

num=[0,1,-1]
while running:
    screen.fill(backgroundColor)
    pygame.draw.rect(screen,stripcolor , (0, 0, strip_color, 70))
    pygame.draw.rect(screen, stripcolor, (0, 166, strip_color, 70))
    pygame.draw.rect(screen, stripcolor, (0, 332, strip_color, 70))
    pygame.draw.rect(screen, stripcolor, (0, 498, strip_color, 70))
    pygame.draw.rect(screen, stripcolor, (0, 664, strip_color, 70))
    pygame.draw.rect(screen, stripcolor, (0, 830, strip_color, 70))

    for event in pygame.event.get():

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                playerX_change = num[-1]
            if event.key == pygame.K_RIGHT:
                playerX_change = num[1]
            if event.key == pygame.K_UP:
                playerY_change = num[-1]
            if event.key == pygame.K_DOWN:
                playerY_change = num[1]
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                playerX_change = num[0]
            if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                playerY_change = num[0]

        if event.type == pygame.QUIT:
            running = False

    if playerX <= num[0]:
        playerX = num[0]
    if playerX >= 1536:
        playerX = 1536
    if playerY <= num[0]:
        playerY = num[0]
        if flag == num[0]:
            flag = num[1]
            count+=num[1]
            playerImg = player2
            r1p1w = True

    if playerY >= 846:
        playerY = 846
        if flag == num[1]:
            flag = num[0]
            count+=num[1]
            playerImg = player1
            r1p2w = True

    for i in range(num_of_enemies):

        if i < 5:
            if count < 2:
                enemy(enemyX[i], enemyY[i+num[0]], i)
                enemyX[i] = enemyX[i] + 4
            elif r1p1w and flag == 0:
                enemy(enemyX[i], enemyY[i+num[0]], i)
                enemyX[i+num[0]] += 7
            elif r1p2w and flag == 1:
                enemy(enemyX[i], enemyY[i+num[0]], i)
                enemyX[i+num[0]] += 7
            else:
                enemy(enemyX[i], enemyY[i+num[0]], i)
                enemyX[i+num[0]] += 4

            if enemyX[i] >= 1546:
                enemyX[i] = num[0]

        else:
            enemy2(enemyX[i+num[0]], enemyY[i], i)
        collision = isCollision(enemyX[i], enemyY[i+num[0]], playerX, playerY)

        check_var2=num[1]
        if collision:


            if flag!=num[0]:
                playerY = 840
                count += 1
                flag = 0
                playerImg = player1
                playerX = 760

            else:
                playerY = 0
                count += 1
                flag = 1
                playerX = 760
                playerImg = player2
            
            print(count)      

        if flag == 1:
            score_valuep1 = 0
            if playerY == enemyY[i]:

                score_valuep2 = score1[i]

            r22 = score_valuep2

        if flag == 0:
            score_valuep2 = 0
            if playerY == enemyY[i]:

                score_valuep1 = score2[i]
                
                r21 = score_valuep1

        if count == 4:
            game_end_text()
            running = False

        elif count == 3:
            t2p2 = (int)((pygame.time.get_ticks()) / 1000 - t2p1 - t1p2 - t1p1)

        elif count == 2:
            r22 = 0
            t2p1 = (int)((pygame.time.get_ticks()) / 1000 - t1p2 - t1p1)

        elif count == 1:
            prevsp2 = score_valuep2
            r21 = 0
            t1p2 = (int)((pygame.time.get_ticks()) / 1000 - t1p1)

        elif count == 0:
            prevsp1 = score_valuep1
            t1p1 = (int)((pygame.time.get_ticks()) / 1000)
        
        
        
        
    print(count)
    show_scorep1(10, 10)
    show_scorep2(1425, 850)
    playerX += playerX_change
    playerY += playerY_change
    player(playerX, playerY)
    pygame.display.update()
